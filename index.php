<?php

session_start();



if( 
	( $_GET['startover'] == "true") || (
		$_SESSION['stats']['char']['current']['hp'] < 1 && 
  		isset( $_SESSION['stats']['char']['current']['hp'] ) ) ){
	session_destroy();

	if($_SESSION['stats']['char']['current']['hp'] < 1){
		echo "You ran out of HP, Game Over!<br /><br />";
	}

	die("<a href=\"tactical.php\">New Game</a>");
}

echo "<h3>Tactical Alliance</h3>";

?>
<script src="shortcut.js" type="text/javascript"></script>
<script type="text/javascript">
<!--
<?php
// Movement shortcuts
?>
shortcut.add("up",function(){
window.location = "?move=u"
});

shortcut.add("down",function(){
window.location = "?move=d"
});

shortcut.add("left",function(){
window.location = "?move=l"
});

shortcut.add("right",function(){
window.location = "?move=r"
});

<?php
// Bomb Placment Shortcuts
?>
shortcut.add("w",function(){
window.location = "?bomb=up"
});

shortcut.add("s",function(){
window.location = "?bomb=down"
});

shortcut.add("a",function(){
window.location = "?bomb=left"
});

shortcut.add("d",function(){
window.location = "?bomb=right"
});

//-->
</script>
<?php

// default starting position
if(!isset($_SESSION['user_locationx'])){$_SESSION['user_locationx'] = "5";}
if(!isset($_SESSION['user_locationy'])){$_SESSION['user_locationy'] = "5";}

// starting health
if(!isset($_SESSION['stats']['char']['current']['hp'])){$_SESSION['stats']['char']['current']['hp'] = 100;}
if(!isset($_SESSION['stats']['char']['current']['mp'])){$_SESSION['stats']['char']['current']['mp'] = 100;}

// set the limits of the playing field, 10x40
$field_limit['t'] = "0";
$field_limit['b'] = "9";
$field_limit['l'] = "0";
$field_limit['r'] = "39";

// Set The Playing Field
$field['x0']= "----------------------------------------";
$field['x1']= "-                                      -";
$field['x2']= "-                                      -";
$field['x3']= "-                                      -";
$field['x4']= "-                                      -";
$field['x5']= "-                                      -";
$field['x6']= "-                                      -";
$field['x7']= "-                                      -";
$field['x8']= "-                                      -";
$field['x9']= "----------------------------------------";
//$field['x10']= "1234567890123456789012345678901234567890";
//$field['x11']= "         1         2         3         4";

function get_user_location(){
	return(array("x" => $_SESSION['user_locationx'], "y" => $_SESSION['user_locationy']));
}

function get_user_inventory($item){
	if( empty($_SESSION['stats']['inventory']['items'][$item]) || !isset($_SESSION['stats']['inventory']['items'][$item]) ){
		$_SESSION['stats']['inventory']['items'][$item] = 0
	}
	return($_SESSION['stats']['inventory']['items'][$item]);
}

function adjust_inventory($item,$quantity){
	get_user_inventory($item);
	$_SESSION['stats']['inventory']['items'][$item] = $_SESSION['stats']['inventory']['items'][$item] + $quantity;
}

function add_inventory($item,$quantity = 1){
	adjust_inventory($item,$quantity);
}

function subtract_inventory($item,$quantity = 1){
	
	$inv = get_user_inventory($item);
	
	if($inv >= $quantity){
		adjust_inventory($item,($quantity*-1));
	}else{
		$msg[] = "You do not have enough of this item: $item.";
	}
}


function set_field_entity($x,$y,$item){

	global $msg;
	$_SESSION['field_entities'][$x][$y][] = $item;

}

function get_field_entities(){
	global $msg;
	return($_SESSION['field_entities']);

}


function place_bomb($dir){
	
	global $field_limit;

	// Set default execution
	$execute = true;

// First Check Invetory

	// Get current bomb inventory
	$inv = get_user_inventory("bomb");
	
	// If not enough inventory, don't execute action
	if($inv < 1){
		$execute = false;
	}

// Now many sure the bomb is being placed inside of the field boundaries

	// Get players current location
	$loc = get_user_location();

	// Set placement reference point
	$placement['x'] = $loc['x'];
	$placement['y'] = $loc['y'];

	// Check if bomb is placed within the boundaries
	if($dir == "up"){
		if($loc['x'] <= $field_limit['t']){
			$execute = false;
			$msg[] = "Can not place bomb out of map.";
		}else{
			$placement['x']--;
		}
	}elseif($dir == "down"){
		if($loc['x'] >= $field_limit['b']){
			$execute = false;
			$msg[] = "Can not place bomb out of map.";
		}else{
			$placement['x']++;
		}
	}elseif($dir == "left"){
		if($loc['y'] <= $field_limit['l']){
			$execute = false;
			$msg[] = "Can not place bomb out of map.";
		}else{
			$placement['y']--;
		}
	}elseif($dir == "right"){
		if($loc['y'] >= $field_limit['r']){
			$execute = false;
			$msg[] = "Can not place bomb out of map.";
		}else{
			$placement['y']++;
		}
	}
	
	if($execute == true){
		// Adjust Inventory
		subtract_inventory("bombs",1)
		
		// Set the bomb
		$_SESSION['bombs'][] = $placement['x']."|".$placement['x'];
	}

}

function get_bomb_locations(){
	return($_SESSION['bombs']);
}



function set_user_location($x,$y){

	global $field_limit;

	if($x > $field_limit['t'] && 
	   $x < $field_limit['b'] && 
	   $y > $field_limit['l'] && 
	   $y < $field_limit['r']){
		// check and set your movement
		$_SESSION['user_locationx'] = $x;
		$_SESSION['user_locationy'] = $y;
	}else{
		return(false);
	}

}

function move_player($dir){
	
	// Get players current location
	$loc = get_user_location();
	
	if($dir == "u"){$loc['x']--;}
	if($dir == "d"){$loc['x']++;}
	if($dir == "l"){$loc['y']--;}
	if($dir == "r"){$loc['y']++;}

	set_user_location($loc['x'],$loc['y']);

}

// Start processing game elements (The "Loop", if you will, of the PHP world)

// Process placing new bombs
place_bomb($_GET['bomb']);

// Move the Player
move_player();

function adjust_user_status($stat,$amount){

	if($stat == "hp"){
		$_SESSION['stats']['char']['current']['hp'] += $amount;
	}
}

function check_field($x,$y){
	global $field;
	return($field['x'.$x][$y]);
}

function set_field($x,$y,$set){
	global $field;
	$field['x'.$x][$y] = $set;
}

function remove_bomb($x,$y){
	// loop through bomb array and remove bomb with matching coordinates.
	foreach ($_SESSION['bombs'] as $k => $v){
		if($v == $x."|".$y){
			unset($_SESSION['bombs'][$k]);
		}
	}	
}


function detection(){

	// Get the users current location
	$loc = get_user_location();

	if(check_field($loc['x'],$loc['y']) == "*"){
		$msg[] = "You hit your own bomb! -10 HP";
		set_field($loc['x'],$loc['y'],"-") // set field to display a blank space (-)
		adjust_user_status("hp","-10");
		remove_bomb($loc['x'],$loc['y']);
	}

}


// Check if the user has collided with anything
detection();


function generate_place_item(){
	
	// Randomly select an item
	$item_var = rand(0,10);

	// Randomly select a location to place said item
	$ix = rand($field_limit_t,$field_limit_b);
	$iy = rand($field_limit_l,$field_limit_r);

	// Place the item...
	if($item_var == "0"){$item = "gold";}
	if($item_var == "1"){$item = "item";}
	if($item_var == "2"){$item = "health";}
	if($item_var == "3"){$item = "bomb";}

	if(!empty($item)){
		set_field_entity($ix,$iy,$item);
	}

}

generate_place_item();



	// Items
	if(!empty($_SESSION['item_location'][0])){
		$i = explode("|",$_SESSION['item_location'][0]);
		if($field['x'.$i['0']][$i['1']] == "X"){
			$_SESSION['stats']['inventory']['items']['potions']++;
			$msg[] = "You collected a Potion!";
			unset($_SESSION['item_location']);
		}elseif($field['x'.$i['0']][$i['1']] != "-"){
			unset($_SESSION['item_location']);
		}else{
			//display item marker
			$field['x'.$i['0']][$i['1']] = "p";
		}
	}

// Gold
if(!empty($_SESSION['gold_location'][0])){
$g = explode("|",$_SESSION['gold_location'][0]);
if($field['x'.$g['0']][$g['1']] == "X"){
$_SESSION['stats']['inventory']['money']['gold']++;
$msg[] = "You collected a Gold Coin!";
unset($_SESSION['gold_location']);
}elseif($field['x'.$g['0']][$g['1']] != "-"){
unset($_SESSION['gold_location']);
}else{
//display item marker
$field['x'.$g['0']][$g['1']] = "G";
}
}

// Health
if(!empty($_SESSION['health_location'][0])){
$h = explode("|",$_SESSION['health_location'][0]);
if($field['x'.$h['0']][$h['1']] == "X"){
$_SESSION['stats']['char']['current']['hp'] = $_SESSION['stats']['char']['current']['hp'] + 20;
$msg[] = "You collected a First Aid Pack!";
if($_SESSION['stats']['char']['current']['hp'] > 100){$_SESSION['stats']['char']['current']['hp'] = 100; $msg[] = "You are at full health!";}
unset($_SESSION['health_location']);
}elseif($field['x'.$h['0']][$h['1']] != "-"){
unset($_SESSION['health_location']);
}else{
//display item marker
$field['x'.$h['0']][$h['1']] = "+";
}
}

// Bombs
if(!empty($_SESSION['bomb_location'][0])){
$b = explode("|",$_SESSION['bomb_location'][0]);
if($field['x'.$b['0']][$b['1']] == "X"){
$_SESSION['stats']['inventory']['items']['bombs']++;
$msg[] = "You collected a Bomb!";
unset($_SESSION['bomb_location']);
}else{
//display item marker
$field['x'.$b['0']][$b['1']] = "B";
}
}



// Randomly place an enemy at the edge of the screen only a % of the time
if(rand(0,15) == "7"){
$rx = rand(0,10);
$ry = rand(0,39);

$re = rand(1,4);
if($re == "1"){$_SESSION['enemies'][] = "0|$ry";}
if($re == "2"){$_SESSION['enemies'][] = "$rx|0";}
if($re == "3"){$_SESSION['enemies'][] = "9|$ry";}
if($re == "4"){$_SESSION['enemies'][] = "$rx|39";}

}

// Make the enemy(s) move towards the user, check for bomb hit, check for user hit.
if(!empty($_SESSION['enemies'])){
	foreach($_SESSION['enemies'] as $k => $v){

		// move enemy towards user, one axis at a time.
		$el = explode("|",$v);

		// enemy can move diagnally, one space up/down OR one space left/right, randonly choosen :D
		$rm = rand(0,1);
		if($rm == "0"){
			if($el[0] < $_SESSION['user_locationx']){$el[0]++;}
			if($el[0] > $_SESSION['user_locationx']){$el[0]--;}
		}else{
			if($el[1] < $_SESSION['user_locationy']){$el[1]++;}
			if($el[1] > $_SESSION['user_locationy']){$el[1]--;}
		}

		$enemy_location = $el[0]."|".$el[1];

		// save enemies location
		$_SESSION['enemies'][$k] = $enemy_location;

		// check if enemy has landed on a bomb
		if($field['x'.$el[0]][$el[1]] == "*"){
			// remove enemy from the board
			unset($_SESSION['enemies'][$k]);
			$msg[] = "Enemy hit a bomb and was defeated!";
			//remove bomb from field...

			foreach ($_SESSION['bombs'] as $kk => $vv){
				$b = explode("|",$vv);
				if($b[0] == $el[0] && $b[1] == $el[1]){unset($_SESSION['bombs'][$kk]);}
			}

			// Give user exp
			$_SESSION['stats']['char']['exp'] = $_SESSION['stats']['char']['exp'] + 127;
			$msg[] = "You gained 127 Exp!";
			// check user exp and level up if necessary / increase HP / MP, etc.
			// ???!!!
		}

		// check if enemy has landed on a user
		if($field['x'.$el[0]][$el[1]] == "X"){
			// remove enemy
			unset($_SESSION['enemies'][$k]);
			// take away your life
			$_SESSION['stats']['char']['current']['hp'] = $_SESSION['stats']['char']['current']['hp']-25;
			$msg[] = "An enemy has hit you! -25 HP";
		}

		// place enemy on the map!
		$field['x'.$el[0]][$el[1]] = "@";

	}
}


function renderMap(){

	global $field;
	global $field_limit;

	// Get bomb locations
	$bombs = get_bomb_locations();

	// Place bombs into the Map
	if(count($bombs) > 0){
		foreach ($bombs as $k => $v){
			$b = explode("|",$v);
			$field['x'.$b[0]][$b[1]] = "*";
		}
	}

	// Set users location on the map, user is represented by a capitol X.
	$loc = get_user_location();
	$field['x'.$loc['x']][$loc['y']] = "X";



}


renderMap();

// Print the field
echo "<pre style=\"border:0px solid #000000; margin:0px auto; width:320px;\">";
foreach ($field as $k => $v){
echo "$v\n";
}
echo "</pre><br /><br />";


// Controls and Information
echo "<center>";
echo "<a href=\"?move=u\">Up</a><br />";
echo "<a href=\"?move=l\">Left</a> - ";
echo "<a href=\"?move=r\">Right</a><br />";
echo "<a href=\"?move=d\">Down</a><br /><br />";

if(!empty($msg)){
echo "<ul>";
foreach ($msg as $k => $v){
echo "<li>$v</li>";
}
echo "</ul>";
}

if(empty($_SESSION['stats']['inventory']['money']['gold'])){$_SESSION['stats']['inventory']['money']['gold'] = 0;}
if(empty($_SESSION['stats']['inventory']['items']['potions'])){$_SESSION['stats']['inventory']['items']['potions'] = 0;}
if(empty($_SESSION['stats']['char']['level'])){$_SESSION['stats']['char']['level'] = 0;}
if(empty($_SESSION['stats']['char']['exp'])){$_SESSION['stats']['char']['exp'] = 0;}
if(empty($_SESSION['stats']['inventory']['items']['bombs'])){$_SESSION['stats']['inventory']['items']['bombs'] = 2;}

echo "<div style=\"width:600px; height:100px; border:1px solid #000000;\">";
echo "<div style=\"float:left; width:198px;\">";
echo "Gold: <b>".$_SESSION['stats']['inventory']['money']['gold']."</b><br />";
echo "Potion: <b>".$_SESSION['stats']['inventory']['items']['potions']."</b><br />";
echo "Bombs: <b>".$_SESSION['stats']['inventory']['items']['bombs']."</b><br />";
echo "Drop Bomb: <a href=\"?bomb=up\">U</a> <a href=\"?bomb=down\">D</a> <a href=\"?bomb=left\">L</a> <a href=\"?bomb=right\">R</a><br />";
echo "</div>";

echo "<div style=\"float:left; width:198px;\">";
echo "HP: <b>".$_SESSION['stats']['char']['current']['hp']."</b><br />";
echo "MP: <b>".$_SESSION['stats']['char']['current']['mp']."</b><br /><br />";
echo "</div>";

echo "<div style=\"float:left; width:198px;\">";
echo "Level: <b>".$_SESSION['stats']['char']['level']."</b><br />";
echo "Exp: <b>".$_SESSION['stats']['char']['exp']."</b><br /><br />";
echo "</div>";

echo "</div>";
echo "<a href=\"?startover=true\">Reset Game</a>";
echo "</center>";

?>